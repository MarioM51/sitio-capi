

```js

{
  title: "Museo De La Madera Madem",
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer consectetur non sapien quis euismod. Sed ut faucibus diam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque gravida sapien risus, ac ornare tellus sodales sed. Phasellus consectetur nisi",
  phone: "241-123-12-12",
  email: "museodelamaderamadem@gmail.com",
  location: "Gabriel M. hernandez No. 12, Tlaxco, Tlaxcala",
  foundation: "28 de Enero de 1968",
  history: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer consectetur non sapien quis euismod. Sed ut faucibus diam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque gravida sapien risus, ac ornare tellus sodales sed. Phasellus consectetur nisi id sodales ornare. Aliquam neque ex, blandit a viverra egestas, lobortis in enim. Aliquam erat volutpat. Maecenas efficitur dictum urna",
  events: [
    {
      name: "Ignaguracion del museo de la madera Madem",
      date: "2020/01/28",
      place: "Gabriel M. hernandez No. 12, Tlaxco, Tlaxcala"
    },{
      name: "Feria del pulque",
      date: "2020/01/28",
      place: "Centro de tlaxco, tlaxcala"
    },
  ]
  
  social_networks: [ {
      name; "Facebook",
      link: "facebook.com",
      icon: "facebook.png"
    }, {
      name; "YouTube",
      link: "youtube.com",
      icon: "youtube.png"
    },{
      name; "Twitter",
      link: "twitter.com",
      icon: "twitter.png"
    }
  ]
  
  principales: [
    {
      name: "carro de madera",
      description: "Ven a ver el tlaxco-movil y como fue fabricado aquí mismo",
      url: "carro-de-madera",
      image: "principales-tlaxco-mobil.png"
    },{
      name: "Tienda",
      description: "Ve la variedad de productos que tenemos a la venta, por ejemplo, tequileros, tablas de cosina etc",
      url: "tienda",
      image: "principales-tienda.png"
    },{
      name: "Atracciones",
      description: "Aparte del tlaxco-movil tenemos atracciones, como automatas, objetos historicos, etc",
      url: "atracciones",
      image: "principales-atracciones.png"
    },{
      name: "Carpinteria",
      description: "Somos una carpinteria que se convirtio en atraccion turistica, pero aun hacemos muebles, maquila madera, etc.",
      url: "carpinteria",
      image: "principales-carpinteria.png"
    }
  ],
  
  productos; [
    {
      name: "Trompo Mediano",
      description: "Trompo de 13cm con cuenda listo para jugar",
      url: "trompo-mediano",
      image: "productos-trompo-mediano.png"
    }, {
      name: "Tequilero",
      description: "para hecharse unos caballitos de 120ml",
      url: "tequilero",
      image: "productos-tequilero.png"
    }, {
      name: "Frutero",
      description: "para poner tus frutas a la vista",
      url: "frutero",
      image: "productos-frutero.png"
    }, {
      name: "Cruz religioza chica",
      description: "Cruz de sabino con una greca incrustada con la tecnica de tarasea",
      url: "cruz-chica",
      image: "productos-cruz-chica.png"
    }, {
      name: "Tabla de cocina",
      description: "Cruz de sabino con una greca incrustada con la tecnica de tarasea",
      url: "cruz-chica",
      image: "productos-cruz-chica.png"
    }
  ]
  
}
```