package com.mario51.museomaderamadem.controller.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.mario51.museomaderamadem.entity.ProductoEntity;
import com.mario51.museomaderamadem.service.IProductoService;

@RestController
@RequestMapping("/api/productos")
public class ProductoControllerImpl {
	Logger logger = LoggerFactory.getLogger(ProductoControllerImpl.class);
	
	@Autowired
	private IProductoService productoService;
	
	@PostConstruct
	private void onPostConstruct() { logger.info("---- created api/productos/"); }
	
	@RequestMapping(path={"", "/"}, method=RequestMethod.GET)
	public List<ProductoEntity> index() {
		return productoService.findAll();
	}
	
	@PostMapping(path={"", "/"})
	@ResponseStatus(HttpStatus.CREATED)
	public ProductoEntity add(@RequestBody ProductoEntity nuevoProducto) {
		return productoService.save(nuevoProducto);
	}
	
	@RequestMapping(path="/filter", method=RequestMethod.GET)
	public List<ProductoEntity> filter(@RequestParam(name="inLandingPage", required=false, defaultValue="") String inLandingPage) {
		boolean inLandingPageb = false; 
		if(inLandingPage.equals("true")) {
			inLandingPageb = true;
		}
		return productoService.filterByInLandingPage(inLandingPageb);
	}
	
	
	@PostMapping("/{idProducto}/uploadImage")
	public ResponseEntity<?> upload(
			@PathVariable Long idProducto,
			@RequestParam("file") MultipartFile file) {
		Map<String, Object> response = new HashMap<String, Object>();

		// Encontrar producto a cual subirle el producto
		ProductoEntity productoToAddImage =  productoService.findById(idProducto);
		if(productoToAddImage == null) {
			response.put("error", "No se encontro el producto a cual agregarle la foto");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		//Subir foto
		if(file.isEmpty()) {
			response.put("error", "No se encontro imagen a subir en la peticion");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		String fileExtension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
		String nombreArchivo = UUID.randomUUID().toString() + "_" + (productoToAddImage.getName().replace(" ", "-")) + fileExtension;
		Path rutaArchivo = Paths.get("front-office/static/images").resolve(nombreArchivo).toAbsolutePath();
		try {
			Files.copy(file.getInputStream(), rutaArchivo);
		} catch (IOException e1) {
			e1.printStackTrace();
			response.put("mensaje", "Error al subir la imagen");
			response.put("descripcion", e1.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		//agregamos foto recien subida al producto obtenido del id entrante
		productoToAddImage.setImage(nombreArchivo);
		ProductoEntity productoActualizado = productoService.save(productoToAddImage);
		if(productoActualizado == null) {
			response.put("error", "No se actualizo el producto");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
				
		response.put("mensaje", "Se subio correctamente la foto " + nombreArchivo);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	
	/*
	 * CONSTRUCCION DE GATSBY 
	*/
	@RequestMapping(path="/build", method=RequestMethod.GET)
	public ResponseEntity<?> build() {
		Map<String, Object> response = new HashMap<String, Object>();
		
		boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");
		
		String ruta = Paths.get("front-office/").toAbsolutePath().toString();
		logger.info("---- To:" + ruta);
				
		
		ProcessBuilder processBuilder = new ProcessBuilder();
        // Windows
        if(isWindows) {
        	String changepartition = "e:";
        	String changeDir = "cd " + ruta;
        	//String build = "ping -n 3 google.com";%CD%
        	String build = "gatsby build";
        	processBuilder.command("cmd.exe", "/c", changepartition + " && " + changeDir + " && " + build);
        } else {
        	response.put("mensaje", "Sistema linux no soportado aun");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
		
        String line = null;
        try {
            Process process = processBuilder.start();
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            /*while ((reader.readLine()) != null) {
            	line += reader.readLine() + System.getProperty("line.separator"); 
            }
           */
            while ((line = reader.readLine()) != null) {
            	System.out.println(line);
            }
            

            int exitCode = process.waitFor();
            System.out.println("\nExited with error code : " + exitCode);

        } catch (IOException e) {
            e.printStackTrace();
            response.put("mensaje", "Error IO");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR); 
        } catch (InterruptedException e) {
            e.printStackTrace();
            response.put("mensaje", "Error interrupcion");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

		response.put("resultado", line);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	
}