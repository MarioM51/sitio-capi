package com.mario51.museomaderamadem.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.mario51.museomaderamadem.entity.ProductoEntity;


public interface IProductoDao extends CrudRepository<ProductoEntity, Long>{
	
	@Query("select p from ProductoEntity p where p.inLandingPage = ?1")
	List<ProductoEntity> filterByInLandingPage(Boolean inLandingPage);
}
