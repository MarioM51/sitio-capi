package com.mario51.museomaderamadem.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mario51.museomaderamadem.dao.IProductoDao;
import com.mario51.museomaderamadem.entity.ProductoEntity;
import com.mario51.museomaderamadem.service.IProductoService;

@Service
public class ClienteServiceImpl implements IProductoService {
	
	Logger logger = LoggerFactory.getLogger(ClienteServiceImpl.class);

	@Autowired
	private IProductoDao productoDao;
	
	@PostConstruct
	private void onPostConstruct() { logger.info("---- created ClienteServiceImpl"); }
	
	@Override
	@Transactional(readOnly = true)
	public List<ProductoEntity> findAll() {
		return (List<ProductoEntity>)productoDao.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<ProductoEntity> filterByInLandingPage(Boolean inLandingpage) {
		return (List<ProductoEntity>)productoDao.filterByInLandingPage(inLandingpage);
	}

	@Override
	@Transactional
	public ProductoEntity save(ProductoEntity nuevoProducto) {
		return productoDao.save(nuevoProducto);
	}

	@Override
	@Transactional(readOnly = true)
	public ProductoEntity findById(Long id) {
		return productoDao.findById(id).orElse(null);
	}
	
}