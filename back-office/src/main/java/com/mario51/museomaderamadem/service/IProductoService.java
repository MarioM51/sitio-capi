package com.mario51.museomaderamadem.service;

import java.util.List;

import com.mario51.museomaderamadem.entity.ProductoEntity;

public interface IProductoService {
	
	List<ProductoEntity> findAll();

	List<ProductoEntity> filterByInLandingPage(Boolean inLandingpage);
		
	ProductoEntity save(ProductoEntity nuevoProducto);
	
	ProductoEntity findById(Long id);
}
