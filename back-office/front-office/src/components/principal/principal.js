import React from "react";
import PrincipalStyles from "./principal.module.css";
import Img from "gatsby-image";

export default (props) => {
  
  const {principalInfo:{name, description, url}, image } = props;
  
  return (
  <article className={PrincipalStyles.principal}>
    <div className={PrincipalStyles.principal_image}>
      <Img fluid={{...image, aspectRatio: 4/4}} alt={name} />
    </div>
    <div className={PrincipalStyles.principal_contenido}>
      <h3>{name}</h3>
      <p>{description}</p>
      <div>
        <a href={url}>Ver</a>
      </div>
    </div>
  </article>
  )
}