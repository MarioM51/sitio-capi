import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Principal from "./principal"
import PrincipalStyles from "./principal.module.css"

export default () => {
  
  const query = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            principales {
              name
              description
              url
              image
            }
          }
        }
        allFile(
          filter: { extension: { regex: "/(jpg)/" } relativeDirectory: { eq: "principales" } }
        ) {
          edges {
            node {
              base
              childImageSharp {
                fixed(width: 250, height: 250) { base64 aspectRatio src srcSet }
              }
            }
          }
        }
      }
    `
  );

  const principalesInfo = query.site.siteMetadata.principales;
  const principalesImages = query.allFile.edges;

  return (
    <div className={PrincipalStyles.principales}>
      {principalesInfo.map((principalInfo, index) => (
        principalesImages.map(imageSrc => (
          (imageSrc.node.base == principalInfo.image) && 
            (<Principal key={index} principalInfo={principalInfo} image={imageSrc.node.childImageSharp.fixed}/>)
        ))
      ))}
    </div>
  )
}


