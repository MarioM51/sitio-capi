import React, { useState, useEffect } from "react"
import "./Carrito.scss"
import CarEmpty from "../../assets/svg/cart-empty.svg";
import CarFull from "../../assets/svg/cart-full.svg";
import CarClose from "../../assets/svg/close.svg";
import CarGarbage from "../../assets/svg/garbage.svg";
import { removeArrayDuplicates, countDuplicatesItemArray } from "../../utils/arrayFunc";
import { STORAGE_RUTE_IMAGES } from "../../utils/Constants";

export default function Carrito(props) {

  const { productsCart, productsView, changeQuantity } = props;
  
  const [ isCarOpen, setIsCarOpen ] = useState(false);
  const [ singleProductsCar, setSingleProductsCar ] = useState([]);
  const widthCarContent = isCarOpen ? 400 : 0;
  
  const [ onAddProduct, setOnAddProduct ] = useState(false);
  const animationShake = onAddProduct ? 'shake 0.5s' : '';

  const openCar = () => { setIsCarOpen(true); /* document.body.style.overflow = "hidden"; */ }
  const closeCar = () => { setIsCarOpen(false); /* document.body.style.overflow = "scroll"; */ }

  useEffect(() => { // de todos los produtos agregados que pueden repetirse, lo dejamos sin repetirce
    const allProductsId = removeArrayDuplicates(productsCart);  
    setSingleProductsCar(allProductsId);
        
    if(allProductsId.length != 0) {
      setOnAddProduct(true);
      setTimeout(() => {
        setOnAddProduct(false);
      }, 500);
    }
  }, [productsCart]);

const [ totalPrice, setTotalPrice ] = useState(0);

useEffect(() => { //calcular el total del carrito
    const productData = [];
    let total = 0;
    const allProductsId = removeArrayDuplicates(productsCart);
    allProductsId.forEach((productId) => {
      const quantity = countDuplicatesItemArray(productId, productsCart);
      const productValue = {
        id: productId,
        quantity: quantity
      };
      productData.push(productValue);
    });
    if(productsView) {
      productsView.forEach(product => {
        productData.forEach(item => {
          if(product.endpointId == item.id) {
            const totalByProduct = product.price * item.quantity;
            total = total + totalByProduct
          }
        });
      });
    }
    setTotalPrice(total);
  }, [productsCart, productsView]);
  
  return (
    <>
      <button className="btn-link car-icon" style={{animation: animationShake}} >
        {(productsCart && productsCart.length>0) ? ( //si hay productos mustra carrito lleno
          <CarFull onClick={openCar} />
        ) : (                       // mustra carrito vacio por que no hay productos
          <CarEmpty onClick={openCar} />
        )}
      </button>
      
      <div className="car-content" style={{width: widthCarContent}}>
        <CarContentHeader closeCar={closeCar} />
        <div className="car-content__products">
          {singleProductsCar.map((idProductCar, index) => ( // iteramos los productos agregados sin repetir
            <CarContentProducts key={index} products={productsView} productsCart={productsCart} idProductCar={idProductCar} changeQuantity={changeQuantity} />
          ))}
        </div>
        <CarContentFooter totalPrice={totalPrice} />
      </div>

    </>
  );
}

function CarContentHeader(props) {
  const { closeCar} = props
  return (
    <div className="car-content__header">
      <h2>Carrito</h2>
      <CarClose onClick={closeCar} />     { /* para cerrar carrito*/ }
    </div>
  )
}

function CarContentProducts(props) {
  const { products, productsCart, idProductCar, changeQuantity } = props;  
  if(products) {
    return products.map((product, index) => { // en todos los productos buscamos el productoId agregado en el carrito
      if(idProductCar == product.endpointId) {     //se encontro un id del carrito en la lista de todos los productos, 1 si existe, 2 sabemos que debemos de pintar este producto
        //contamos cuantas veces esta el mismo producto en el carrito
        const quantity = countDuplicatesItemArray(product.endpointId, productsCart);
        return (
          <RenderProduct key={index} product={product} quantity={quantity} changeQuantity={changeQuantity} />
        )
      }
    });
  }
return null;
}

function RenderProduct(props) { // sin ciencia solo pintamos, lo complicado son los estilos
  const { product, quantity, changeQuantity:{ increseQuantity,  decreseQuantity } } = props

  const imagesPath = localStorage.getItem(STORAGE_RUTE_IMAGES+product.endpointId);

  return (
    <div className="car-content__product" style={{color: 'white'}}>
      <img src={imagesPath + "/"+ product.image}  alt={product.name} style={{width: '90px'}} />
      <div className="car-content__product-info">
        <div><h3>{product.name.substr(0, 25)}</h3></div>
        <p>{product.price.toFixed(2)} $ / ud.</p>
        <div>
          <p>En Carrito: {quantity} ud.</p>
          <div>
            <button onClick={() => increseQuantity(product.endpointId) } >+</button>
            <button onClick={() => decreseQuantity(product.endpointId) } >-</button>
          </div>
        </div>
      </div>
    </div>
  );
}

function CarContentFooter(props) {
  const { totalPrice } = props;
  //console.log("Total Price: ", totalPrice);
  return (
    <div className="car-content__footer">
      <div>
        <p>Total Aproximado: </p>
        <p>{totalPrice.toFixed(2)} $</p>
      </div>
      <button>Tramitar Pedido</button>
    </div>
  )
}

