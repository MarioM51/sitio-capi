import React from "react"
import ProductoStyles from "./producto.module.css"
import Img from "gatsby-image";

export default function Producto(props) {

  const {
    landingPage,
    addToCar,
    product: {url, name, description, price, quantity},
    imageSrc
  } = props;

  //console.log("imageSrc", imageSrc);
  
  return (
    <article className={ProductoStyles.producto}>
      <div className={ProductoStyles.producto_image}>
        <Img fluid={{...imageSrc, aspectRatio: 4/4}} alt={name} />
      </div>
      <div className={ProductoStyles.productos_contenido}>
        <a href={url}>
          <h3>{name}</h3>
          <p>{description}</p>
        </a>
        <div className={ProductoStyles.precio}><span>{price} $</span></div>
        <div className={ProductoStyles.quantity}>Cantidad:&nbsp;<span>{quantity}</span></div>
        {(!landingPage) && (// SE OCULTA EN EL LANDING_PAGE SE MUESTRA EN LA IENDA
        <div className={ProductoStyles.btn_producto_container}>
          <button className="btn_producto" onClick={() => {addToCar(props.product.endpointId, imageSrc)}} >Agregar a carrito</button>
        </div>
        )}
      </div>
    </article>
  )
}