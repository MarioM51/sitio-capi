import React from "react"
import { useStaticQuery, graphql, Link } from "gatsby"
import Producto from "./producto"
import ProductoStyles from "./producto.module.css"
import JSONProducts from "../../data/products.json"

export default () => {
 
  //site { siteMetadata { products { name description url image price inLandingPage } } }
  const query = useStaticQuery(graphql` query {
        allFile(
          filter: { extension: { regex: "/(jpg)/" } relativeDirectory: { eq: "products" } }
        ) {
          edges {
            node {
              base
              childImageSharp {
                fixed(width: 250, height: 250) { base64 aspectRatio src srcSet }
              }
            }
          }
        }
      }
    `
  );

  const productsInLanding = JSONProducts;
  const productsInLandingImages = query.allFile.edges;

  return (
    <div className={ProductoStyles.productos}>
      {productsInLanding.map((product, index) => (
        (product.inLandingPage) && (
          (productsInLandingImages.map((imageSrc) => (
            (imageSrc.node.base === product.image) && (
              <Producto key={index} product={product} imageSrc={imageSrc.node.childImageSharp.fixed} landingPage /> 
            )
          )))
        )
      ))}

      <article className={ProductoStyles.enlace_tienda}>
      <Link to="/tienda">
        <div className={ProductoStyles.productos_contenido}>
          <h3>Ver mas</h3>
          <p>Ver catalogo para ver todos lo productos</p>
        </div>
      </Link>
    </article>
    </div>
    
  )
}


