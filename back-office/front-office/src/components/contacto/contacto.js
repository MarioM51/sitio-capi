import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import ContactStyles from "./contacto.module.css"

export default () => {
  
  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            phone
            location
            email
            social_networks {
              name
              link
              icon
            }
          }
        }
      }
    `
  )
  
  //console.log("Info de contacto", data.site.siteMetadata);

  return (
  <article className={ContactStyles.contacto}>
      <div className={ContactStyles.contacto_info}>
        <h3>Telefono/Whatsapp:</h3>
        <div>{data.site.siteMetadata.phone}</div>
      </div>
      <div className={ContactStyles.contacto_info}>
        <h3>Ubicacion:</h3>
        <div>{data.site.siteMetadata.location}</div>
      </div>

      <div className={ContactStyles.contacto_info}>
        <h3>E-Mail:</h3>
        <div>{data.site.siteMetadata.email}</div>
      </div>

      <div className={ContactStyles.contacto_info}>
        <h3>Redes Sociales</h3>
        <nav className={ContactStyles.sociales}>
          <ul>
            {data.site.siteMetadata.social_networks.map(({name, link}) => (
              <li key={name} ><a href={link}><span>{name}</span></a></li>
            ))}
          </ul>
        </nav>
      </div>
      
  </article>
  )
}


