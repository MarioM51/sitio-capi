import React from "react"
import { Link } from "gatsby"
import containerStyles from "./layout.module.css"
import { useStaticQuery, graphql } from "gatsby"
import ArrowLeftShort from "../../assets/svg/back.svg";
import Carrito from "../carrito/Carrito";

export default (props) => {

  const { children, hideNav, productsCart, productsView, changeQuantity } = props;

  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
          }
        }
      }
    `
  )

  //console.log("Header"); console.log(data.site.siteMetadata);

  return (
  <div>
    <header className={containerStyles.headerContainer}>
        <div className={containerStyles.headerContent} >
        <Link to="/" style={{  textDecoration: `none`, color: `black` }}>
          <div className={containerStyles.headerLeft} >
            {hideNav && ( <ArrowLeftShort size={36} /> ) }
            <h1>{data.site.siteMetadata.title}</h1>
          </div>
          </Link>
          <div className="header-right">
            {hideNav && ( 
              <Carrito productsCart={productsCart} productsView={productsView} changeQuantity={changeQuantity} />
              ) }
          </div>
        </div>
      
      {!hideNav && (
        <nav className={containerStyles.menu_nav}>
        <a href="#principales">Principales</a>
        <a href="#productos">Productos</a>
        <a href="#sobre-nosotros">Sobre Nosotros</a>
        <a href="#contacto">Contacto</a>
      </nav>
      )}

    </header>
    <main>
      {children}
    </main>
    <footer>
      hola desde footer
    </footer>
  </div>
  )
}