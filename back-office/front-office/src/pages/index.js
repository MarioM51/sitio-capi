import React from "react"
import Layout from "../components/layout"
import Principals from "../components/principal/principales"
import ProductosLanding from "../components/producto/productosLanding"
import SobreNosotros from "../components/sobre-nosotros/sobre-nosotros"
import Contacto from "../components/contacto"
import SEO from "../components/seo"
import { useStaticQuery, graphql } from "gatsby"

export default () => {
  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
          }
        }
      }
    `
  )

  //console.log("Index", data.site.siteMetadata);
  
  return(
    <Layout>
      <SEO title={data.site.siteMetadata.title} description={data.site.siteMetadata.description} />
      <section id="#principales" className="container-principales" style={{ marginTop: '4rem'}}>
        <h2>Principales</h2>
        <Principals />
      </section>
      <section id="#productos" className="productos-container">
          <h2>Productos</h2>
          <ProductosLanding />
      </section>
      <section id="#sobre-nosotros" className="sobre-nosotros-container">
        <h2>Sobre Nosotros</h2>
        <SobreNosotros />
      </section>
        
      <section id="#contacto" className="contacto-container">
        <h2>Contacto</h2>
        <Contacto />
      </section>
  </Layout>
  )
}