import React, { useState } from "react"
import Layout from "../components/layout"
import { useStaticQuery, graphql } from "gatsby"
import { STORAGE_PRODUCTS_CAR, STORAGE_RUTE_IMAGES } from "../utils/Constants";

import Producto from "../components/producto/producto"
import ProductoStyles from "../components/producto/producto.module.css"
import { removeItemArray } from "../utils/arrayFunc";
import { ToastContainer, toast } from 'react-toastify';

import JSONData from "../../src/data/products.json"

//<section className="buscador">
//        Pa buscar
//     </section>

export default () => {
  
  const [ productsCart, setProductsCart ] = useState([]);

  const MSG_PRODUCT_ADDED = "Producto añadido al carrito";
  const MSG_PRODUCT_EXHAUSRED = "Producto agotado";

  const addproductToCart = (id, imageSrc) => {
    increseQuantity(id);
    localStorage.setItem(STORAGE_RUTE_IMAGES+id,  imageSrc.src.substr(0, imageSrc.src.lastIndexOf("/") ) );
    //console.log(`Agregado ${imageSrc} con ID ${id}`);
  }

  const getInitialProductsCar = () => {
    const idsProducts = localStorage.getItem(STORAGE_PRODUCTS_CAR);
    if(idsProducts) {
      const idsTemp = idsProducts.split(","); //recuperamos string (ej '1,2,3') lo convertimos a array
      setProductsCart(idsTemp);
    } else {
      setProductsCart([]);
    }
  }

  /*
  const emptyCar = () => {
    localStorage.removeItem(STORAGE_PRODUCTS_CAR);
    getInitialProductsCar();
  }
  */

  const increseQuantity = (id) => {
    console.log("decremento");
    
    const inExistence = changeQuantityOnProductsView(id, false);
    if(inExistence) {
      //agregamos en orden por ejemplo 1,1,1,4,4,4,5,5,2 los numeros iguales estan juntos
      const indexToInsert = productsCart.lastIndexOf(id+"");
      productsCart.splice(indexToInsert, 0, id);

      localStorage.setItem(STORAGE_PRODUCTS_CAR, productsCart);
      getInitialProductsCar();
      toast.success(MSG_PRODUCT_ADDED);
    } else {
      toast.error(MSG_PRODUCT_EXHAUSRED);
    }
  }

  const decreseQuantity = (id) => {
    changeQuantityOnProductsView(id, true);

    const arrayItemCar = productsCart;
    localStorage.setItem(STORAGE_PRODUCTS_CAR, removeItemArray(arrayItemCar, id.toString()));
    getInitialProductsCar();
  }

  const changeQuantity = { increseQuantity,  decreseQuantity };

  /*
  useEffect(() => {
    getInitialProductsCar();
  }, []);
  */
 
 // site { siteMetadata { products { endpointId, name description url image price inLandingPage quantity } } }
  const data = useStaticQuery(graphql`query {
      allFile( filter: { extension: { regex: "/(jpg)/" } relativeDirectory: { eq: "products" } } ) {
        edges {
          node {
            base
            childImageSharp {
              fixed(width: 250, height: 250) { base64 aspectRatio src srcSet }
            }
          }
        }
      }
    }
  `
);

  const productsViewQuery = JSONData;
  const [ productsView, setProductsView ] = useState(productsViewQuery);

  const changeQuantityOnProductsView = (id, increse) => {
    var indexSelected = productsView.findIndex(product => { return product.endpointId === id });
    const productSelected = productsView[indexSelected];
    
    if(increse) {
      productSelected.quantity = productSelected.quantity + 1;
      return true;
    } else {
      if(productSelected.quantity > 0) {
        productSelected.quantity = productSelected.quantity - 1;
        return true;
      }
    }
    return false;
  }

  const productsInLandingImages = data.allFile.edges;

  //<Layout hideNav emptyCar={emptyCar} productsCart={productsCart} productsView={productsView} changeQuantity=
  return(
    <Layout hideNav productsCart={productsCart} productsView={productsView} changeQuantity={changeQuantity}>
      <section className="productos-container">
          <h2>Productos</h2>
          <div className={ProductoStyles.productos_tienda}>
            {productsView.map((product, index) => (
              (productsInLandingImages.map((imageSrc) => (
                (imageSrc.node.base === product.image) && (
                  <Producto key={index} product={product} addToCar={addproductToCart} imageSrc={imageSrc.node.childImageSharp.fixed} />
                )
              )))
            ))}
          </div>

          <ToastContainer
            position="bottom-left" autoClose={4000} hideProgressBar
            newestOnTop closeOnClick rtl={false} pauseOnHover={false}
            pauseOnVisibilityChange={false} draggable
          />
          
      </section>
  </Layout>
  )

}