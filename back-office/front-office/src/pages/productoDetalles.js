import React from "react"
import Layout from "../components/layout"

import styles from "./producto-detalles.module.scss"

export default () => {
    
  return(
    <Layout hideNav>
      <article className={styles.producto_detalles_container}>
        <div className={styles.imagenes} >
          <img src="./images/amarillo.png" alt="Prueba" />
          <div className={styles.small_images}>
            <img src="./images/gris.png" alt="Prueba" onClick="" />
            <img src="./images/gris.png" alt="Prueba" onClick="" />
            <img src="./images/gris.png" alt="Prueba" onClick="" />
            <img src="./images/gris.png" alt="Prueba" onClick="" />
            <img src="./images/gris.png" alt="Prueba" onClick="" />
          </div>
        </div>
        <div className={styles.info_principal} >
          <h2>Trompo de madera chico, mas texto para un segundo parrafo</h2>
          <span>$ 99,999.00</span>
          <button className="btn_producto">Agregar al Carrito</button>
          <span  className={styles.disponibles} >Unidades Disponibles <b>100</b></span>
        </div>
        <div className={styles.descripcion} >
          <h2>Descripcion</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sit amet orci nisl. Praesent porttitor hendrerit nunc, nec porta erat. Vivamus pretium, sapien vitae scelerisque congue, est quam interdum lorem, vel dapibus nisi orci id nisi. Curabitur laoreet iaculis augue in sagittis. Quisque nec nibh congue, posuere massa nec, u</p>
        </div>

        <div className={styles.caracteristicas} >
          <h2>Caracteristicas</h2>
          <div>
            <div>
              <span>Propiedad</span>
              <span>Valor</span>
            </div>
            <div>
              <span>Propiedad</span>
              <span>Valor</span>
            </div>
            <div>
              <span>Propiedad</span>
              <span>Valor</span>
            </div>
            <div>
              <span>Propiedad</span>
              <span>Valor</span>
            </div>
        </div>
        </div>
      </article>
    </Layout>
  )
}