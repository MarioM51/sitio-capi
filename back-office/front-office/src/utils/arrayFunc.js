//Para contar cuantos elementos dplicados ahi en un array
export const countDuplicatesItemArray = (value, array) => {
  let count = 0;
  array.forEach(arrayValue => {
    if(arrayValue == value) {
      count++;
    }
  });
  return count;
}

//Eliminar elementos repetidos
export const removeArrayDuplicates = array => {
  return Array.from(new Set(array));
}

//eliminar un elemento del array
export const removeItemArray = (array, itemToDelete) => {
  const index = array.indexOf(itemToDelete);
  if(index > -1) { // si encontro elemento a eliminar
    array.splice(index, 1);
  }
  return array;
}