/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  /* Your site config here */
  siteMetadata: {
    title: "Museo De La Madera Madem",
    description: "Hola desde este archivo, consectetur adipiscing elit. Integer consectetur non sapien quis euismod. Sed ut faucibus diam. Class aptent taciti sociosqu",
    phone: "241-123-12-12",
    email: "museodelamaderamadem@gmail.com",
    location: "Gabriel M. hernandez No. 12, Tlaxco, Tlaxcala",
    foundation: "28 de Enero de 1968",
    history: "Que tal perrors, consectetur adipiscing elit. Integer consectetur non sapien quis euismod. Sed ut faucibus diam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque gravida sapien risus, ac ornare tellus sodales sed. Phasellus consectetur nisi id sodales ornare. Aliquam neque ex, blandit a viverra egestas, lobortis in enim. Aliquam erat volutpat. Maecenas efficitur dictum urna",
    author: "Mario Marquez",
    keywords: [ "carpinteria", "museo de la madera", "atraccion turistica", "artesanias", "automatas", "carro de madera"],
    events: [
      {
        name: "Ignaguracion del museo de la madera Madem",
        date: "2020/01/28",
        place: "Gabriel M. hernandez No. 12, Tlaxco, Tlaxcala"
      },{
        name: "Feria del pulque",
        date: "2020/01/27",
        place: "Centro de tlaxco, tlaxcala"
      },
    ],
    
    social_networks: [ {
        name: "Facebook",
        link: "facebook.com",
        icon: "facebook.png"
      }, {
        name: "YouTube",
        link: "youtube.com",
        icon: "youtube.png"
      },{
        name: "Twitter",
        link: "twitter.com",
        icon: "twitter.png"
      }
    ],
    
    principales: [
      {
        name: "carro de madera",
        description: "Ven a ver el tlaxco-movil y como fue fabricado aquí mismo",
        url: "carro-de-madera",
        image: "carrito.jpg"
      },{
        name: "Tienda",
        description: "Ve la variedad de productos que tenemos a la venta, por ejemplo, tequileros, tablas de cosina etc",
        url: "tienda",
        image: "tienda.jpg"
      },{
        name: "Atracciones",
        description: "Aparte del tlaxco-movil tenemos atracciones, como automatas, objetos historicos, etc",
        url: "atracciones",
        image: "titeres.jpg"
      },{
        name: "Carpinteria",
        description: "Somos una carpinteria que se convirtio en atraccion turistica, pero aun hacemos muebles, maquila madera, etc.",
        url: "carpinteria",
        image: "trompo.jpg"
      }
    ],
  },

  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    'gatsby-image',
    `gatsby-plugin-offline`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Museo de la madera MADEM`,
        short_name: `MADEM`,
        start_url: `/`,
        background_color: `#6b37bf`,
        theme_color: `#6b37bf`,
        display: `standalone`,
        icon: `src/assets/images/favicon.ico`, // This path is relative to the root of the site.
      },
    }, {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /assets/
        }
      }
    }, {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`, path: `${__dirname}/src/assets/images/`,
      },
    },
  ]
}
